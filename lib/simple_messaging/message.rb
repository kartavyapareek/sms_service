module SimpleMessaging
  class Message

    attr_accessor :phone, :adapter, :content

    def deliver
      log_to_console
      return unless SimpleMessaging.config.perform_deliveries
      adapter.send_message(phone, content)
    end


    private

      def log_to_console
        puts "Simply Messaging content: '#{content}', to: '#{phone}', with adapter: '#{SimpleMessaging.config.adapter}'"
      end

  end
end
