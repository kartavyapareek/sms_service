require 'rails'

module SimpleMessaging
  class Railtie < Rails::Railtie

    config.simple_messaging = ActiveSupport::OrderedOptions.new

    initializer "simple_messaging.initialize" do |app|
      SimpleMessaging.configure do |config|
        config.adapter = app.config.simple_messaging.adapter if app.config.simple_messaging.adapter
        config.perform_deliveries = !Rails.env.development? || app.config.simple_messaging.perform_deliveries
      end
    end

  end
end
