module SimpleMessaging
  class MessageDelivery < Delegator

    def initialize(messenger_class, method_name, *args)
      @messenger_class, @method_name, @args = messenger_class, method_name, args

      messenger = @messenger_class.new @method_name
      messenger.send method_name, *@args
      @message = messenger.message
    end

    def __getobj__
      @message
    end

    def deliver_later(options={})
      enqueue_delivery options
    end

    def deliver_now
      @message.deliver
    end


    private

      def enqueue_delivery(options={})
        args = @messenger_class.name, @method_name.to_s, *@args
        SimpleMessaging::DeliveryJob.set(options).perform_later(*args)
      end

  end
end
