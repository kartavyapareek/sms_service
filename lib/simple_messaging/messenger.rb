module SimpleMessaging
  class Messenger

    attr_accessor :message

    def initialize(method_name)
      @method_name = method_name
      self.message = Message.new
      self.message.adapter = self.class.adapter_class
    end


    protected

      def self.method_missing(method_name, *args)
        if public_instance_methods(false).include? method_name
          MessageDelivery.new self, method_name, *args
        else
          super
        end
      end

      def send_message(to:)
        self.message.content = message_content
        self.message.phone = to
        self.message
      end


    private

      def self.adapter_class
        require "simple_messaging/adapters/#{SimpleMessaging.config.adapter}"
        "SimpleMessaging::Adapters::#{SimpleMessaging.config.adapter.camelize}".constantize.new
      end

      def message_content
        partial_path = self.class.name.underscore.downcase.gsub("::", "/")
        path = Pathname.new "#{Rails.root.to_s}/app/views/simple_messaging/#{partial_path}/#{@method_name.to_s}.text.erb"

        raise "No template found for #{self.class.name} #{@method_name}" unless path.exist?

        template = ERB.new File.open(path).read
        template.result(self.instance_eval { binding })
      end

  end
end