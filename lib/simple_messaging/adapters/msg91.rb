module SimpleMessaging
  module Adapters
    class Msg91

      begin
        @@MSG91_AUTHKEY = Rails.application.config.msg91["authkey"]
        @@MSG91_SENDER = Rails.application.config.msg91["sender"]
      rescue
        raise "Configuration failure for Msg91 adapter"
      end

      def send_message(phone, content)
        adaptee.sendhttp(mobiles: ["91#{phone}"], message: content)
      end

      class API
        def initialize(authkey:, sender:)
          @authkey = authkey
          @sender = sender
        end

        def sendhttp(message:, mobiles:)
          endpoint = "http://api.msg91.com/api/sendhttp.php"
          fixed = "route=4&country=91&sender=#{@sender}&authkey=#{@authkey}"
          result = open("#{endpoint}?#{fixed}&mobiles=#{mobiles.join(',')}&message=#{message}")
          result.try(:status).try(:include?, "200")
        end
      end


      private

        def adaptee
          API.new authkey: @@MSG91_AUTHKEY, sender: @@MSG91_SENDER
        end

    end
  end
end
