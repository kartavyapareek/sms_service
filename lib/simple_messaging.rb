require "simple_messaging/messenger"
require "simple_messaging/delivery_job"
require "simple_messaging/message"
require "simple_messaging/message_delivery"
require "simple_messaging/railtie" if defined? ::Rails::Railtie
require "simple_messaging/version"


module SimpleMessaging

  class << self
    class_attribute :config
    self.config = OpenStruct.new
    self.config.adapter = "gupshup"
    self.config.perform_deliveries = true

    def configure(&block)
      yield self.config
    end
  end

end
