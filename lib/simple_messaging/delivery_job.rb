require 'active_job'

module SimpleMessaging
  class DeliveryJob < ActiveJob::Base
    queue_as :simple_messaging

    def perform(messenger_name, method_name, *args)
      messenger_name.constantize.public_send(method_name, *args).deliver
    end

  end
end
