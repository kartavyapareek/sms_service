module SimpleMessaging
  module Adapters
    class Gupshup

      begin
        @@GUPSHUP_USER_ID = Rails.application.config.gupshup["user_id"]
        @@GUPSHUP_PASSWORD = Rails.application.config.gupshup["password"]
      rescue
        raise "Configuration failure for Gupshup adapter"
      end

      def send_message(phone, content)
        adaptee.send_text_message(send_to: "91#{phone}", msg: content)
      end


      private

        def adaptee
          ::Gupshup::Enterprise.new userid: @@GUPSHUP_USER_ID, password: @@GUPSHUP_PASSWORD
        end

    end
  end
end
